
fn fibo(idx: i32)-> i32 {
    if (idx == 1) || (idx==0) {
        return idx + 2;
    }

    let val1 = fibo(idx - 1);
    let val2 = fibo(idx - 2);

    return val1 + val2;
}

fn main() {
    let mut cnt = 10;
    let mut val0 = 2;
    let mut val1 = 3;
    while cnt > 0 {
        println!(" {} ", val1);

        let next = val1 + val0;
        val0 = val1;
        val1 = next;

        cnt -= 1;
    }

    println!(" fibo 10 {} ", fibo(10)); 
}
